﻿using System;

namespace ConsoleApp
{
	class Program
	{
		static void Main(string[] args)
		{
			const string testUrl = "https://habr.com/ru/company/kaspersky/blog/546582/";
			const string outputPath = "out";
			ImageLoader.UploadImages(testUrl, outputPath).Wait();
		}
	}
}
