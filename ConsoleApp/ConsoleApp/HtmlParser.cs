﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;

namespace ConsoleApp
{
	public static class HtmlParser
	{
		public static List<string> GetImagesUrl(string html)
		{
			var regex = new Regex(@"<img[^>]*\ssrc\s*=\s*['""]?(http[^\s>'""]*)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
			return regex.Matches(html).Select(m => m.Groups[1].Value).ToList();
		}
	}
}
