﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp
{
	public static class ImageLoader
	{
		public static async Task UploadImages(string url, string outputDir)
		{
			var client = new HttpClient();
			var response = await client.GetAsync(url);
			var html = await response.Content.ReadAsStringAsync();
			var imagesUrl = HtmlParser.GetImagesUrl(html);
			var outputPath = Path.Combine(Directory.GetCurrentDirectory(), outputDir);
			Directory.CreateDirectory(outputPath);
			var tasks = imagesUrl.Select(async (url) =>
			{
				var stream = await UploadImage(url);
				var file = File.OpenWrite(Path.Combine(outputPath, GetImageName(url)));
				stream.CopyTo(file);
			});
			await Task.WhenAll(tasks);
		}

		private static async Task<Stream> UploadImage(string url)
		{
			var client = new HttpClient();
			var response = await client.GetAsync(url);
			return await response.Content.ReadAsStreamAsync();
		}

		private static string GetImageName(string url)
		{
			var regex = new Regex(@"[/\\]([^/\\?]*)(?:\?|$)");
			return regex.Match(url).Groups[1].Value;
		}
	}
}
